package cz.esw.serialization.json;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Marek Cuchý
 */
public class Measurements {

	private Map<Integer,Dataset> measurements = new HashMap<>();

	public Dataset add(Dataset dataset) {
		return measurements.put(dataset.getId(), dataset);
	}

	public Map<Integer, Dataset> getMeasurements() {
		return measurements;
	}

	public void setMeasurements(Map<Integer, Dataset> measurements) {
		this.measurements = measurements;
	}
}
