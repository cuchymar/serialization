package cz.esw.serialization.json;

/**
 * @author Marek Cuchý
 */
public class MeasurementInfo {

	private long timestamp;
	private String measurerName;

	public MeasurementInfo() {
	}

	public MeasurementInfo(long timestamp, String measurerName) {
		this.timestamp = timestamp;
		this.measurerName = measurerName;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String getMeasurerName() {
		return measurerName;
	}

	public void setMeasurerName(String measurerName) {
		this.measurerName = measurerName;
	}
}
