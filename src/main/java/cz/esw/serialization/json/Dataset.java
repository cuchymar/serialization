package cz.esw.serialization.json;

import java.util.List;

/**
 * @author Marek Cuchý
 */
public class Dataset {

	private int id;
	private MeasurementInfo info;
	private DataType type;
	private List<Double> values;

	public Dataset() {
	}

	public Dataset(int id, MeasurementInfo info, DataType type, List<Double> values) {
		this.id = id;
		this.info = info;
		this.type = type;
		this.values = values;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public MeasurementInfo getInfo() {
		return info;
	}

	public void setInfo(MeasurementInfo info) {
		this.info = info;
	}

	public DataType getType() {
		return type;
	}

	public void setType(DataType type) {
		this.type = type;
	}

	public List<Double> getValues() {
		return values;
	}

	public void setValues(List<Double> values) {
		this.values = values;
	}
}
