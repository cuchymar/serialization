package cz.esw.serialization;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.esw.serialization.json.DataType;
import cz.esw.serialization.json.Dataset;
import cz.esw.serialization.json.MeasurementInfo;
import cz.esw.serialization.json.Measurements;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Marek Cuchý
 */
public class DataGenerator {

	private static final Random RND = new Random(0);

	public static void main(String[] args) throws IOException {
		String[] names = {"honza", "petr", "michal", "jirka"};
		int iterations = 10000;

		Measurements measurements = new Measurements();
		int id = 0;
		for (String name : names) {
			for (int i = 0; i < iterations; i++) {
				for (DataType dataType : DataType.values()) {
					measurements.add(createDataset(id++, name, System.currentTimeMillis(), dataType, 10));
				}
			}
		}
		ObjectMapper mapper = new ObjectMapper();
		long t1 = System.nanoTime();
		mapper.writeValue(new File("measurements.json"), measurements);
		long t2 = System.nanoTime();

		System.out.println("Saving JSON took " + (t2 - t1) / 1_000_000 + " ms");
	}

	private static Dataset createDataset(int id, String name, long timestamp, DataType dataType, int valuesCount) {
		List<Double> values = new ArrayList<>(valuesCount);
		for (int i = 0; i < valuesCount; i++) {
			double value = -1;
			switch (dataType) {
				case DOWNLOAD:
					value = RND.nextInt(90000) + 10000;
					break;
				case UPLOAD:
					value = RND.nextInt(9000) + 1000;
					break;
				case PING:
					value = RND.nextInt(1000);
					break;
			}
			values.add(value);
		}
		return new Dataset(id, new MeasurementInfo(timestamp, name), dataType, values);
	}

}
